module github.com/karmada-io/karmada

go 1.14

require (
	github.com/evanphx/json-patch/v5 v5.1.0
	github.com/go-logr/logr v0.3.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/onsi/ginkgo v1.12.1
	github.com/blang/semver/v4 v4.0.0
	)
